#pragma once

#ifndef _scanline_
#define _scanline_

void storeEdgeInTable(int x1, int y1, int x2, int y2);
void ScanlineFill(int color);
void initEdgeTable();

#endif