// menuopengl.cpp: define el punto de entrada de la aplicación de consola.
//
#include "stdafx.h"
#include <GL/glut.h>
#include <iostream>
#include <math.h>
#include "Scanline.h"
#define BITMAP_ID 0x4D42
#define _USE_MATH_DEFINES
#define maxHt 500 
#define maxWd 500 
#define maxVer 10000 
GLuint nombres_texturas;

FILE *fp;

static int rotarobj = 0;
static int left = -100;
static int right = 450;
static int bottom = -100;
static int top = 400;
static int nearV = -100;
static int farV = 100;
static int eje = 0;
static int midx = -(left + right) / 2;
static int midy = -(bottom + top) / 2;
static int opcion = 0;
static int velPan = 5;
static int velZoom = 5;
static int velRot = 10;

BITMAPINFOHEADER bitmapInfoHeader;
unsigned char* bitmapData;

unsigned char* LoadBitmapFile(const char *filename, BITMAPINFOHEADER *bitmapInfoHeader)
{
	FILE *filePtr;                       // Puntero a programa
	BITMAPFILEHEADER bitmapFileHeader;   // Bitmap file header
	unsigned char *bitmapImage;          // Bitmap image data
	int imageIdx = 0;             // Index de la imagen
	unsigned char tempRGB;

	// Abrimos el archivo en binary mode
	filePtr = fopen(filename, "rb");
	if (filePtr == NULL)
		return NULL;

	// Leemos el bitmap file header
	fread(&bitmapFileHeader, sizeof(BITMAPFILEHEADER), 1, filePtr);

	// Verificamos que sea un BItmap
	if (bitmapFileHeader.bfType != BITMAP_ID)
	{
		fclose(filePtr);
		return NULL;
	}

	// Leemos la información del bitmap header
	fread(bitmapInfoHeader, sizeof(BITMAPINFOHEADER), 1, filePtr);

	// Movemos el puntero al principio del bitmap
	fseek(filePtr, bitmapFileHeader.bfOffBits, SEEK_SET);

	// Guardamos suficiente memoria para el archivo
	bitmapImage = (unsigned char*)malloc(bitmapInfoHeader->biSizeImage);

	// Verificamos la memoria
	if (!bitmapImage)
	{
		free(bitmapImage);
		fclose(filePtr);
		return NULL;
	}
	fread(bitmapImage, 1, bitmapInfoHeader->biSizeImage, filePtr);
	if (bitmapImage == NULL)
	{
		fclose(filePtr);
		return NULL;
	}

	// Cambiamos la imagen a RGB
	for (imageIdx = 0; imageIdx<(int)bitmapInfoHeader->biSizeImage; imageIdx += 3)
	{
		tempRGB = bitmapImage[imageIdx];
		bitmapImage[imageIdx] = bitmapImage[imageIdx + 2];
		bitmapImage[imageIdx + 2] = tempRGB;
	}

	// Cerramos el archivo y devolvemos la imagen
	fclose(filePtr);
	return bitmapImage;
}

void init(void)
{
	glClearColor(1.0, 1.0, 1.0, 0.0); // Clear the color 
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	
	glShadeModel(GL_FLAT); // Set the shading model to GL_FLAT

	glEnable(GL_TEXTURE_2D); 
	glGenTextures(1, &nombres_texturas); // Genero el nombre de la textura

	glBindTexture(GL_TEXTURE_2D, nombres_texturas); // Activamos la textura
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	bitmapData = LoadBitmapFile("mouse.bmp", &bitmapInfoHeader);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, bitmapInfoHeader.biWidth,
		bitmapInfoHeader.biHeight, GL_RGB, GL_UNSIGNED_BYTE, bitmapData);
}

void reshape(int w, int h)
{
	glViewport(0, 0, (GLsizei)w, (GLsizei)h); // Set the viewport
	glMatrixMode(GL_PROJECTION); 	// Set the Matrix mode
	glLoadIdentity();
	glOrtho(left, right, bottom, top, nearV, farV);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void dibujo()
{
	glColor3f(0.0, 0.0, 0.0);
	int count = 0, x1, y1, x2, y2;
	rewind(fp);
	while (!feof(fp))
	{
		count++;
		if (count > 2)
		{
			x1 = x2;
			y1 = y2;
			count = 2;
		}
		if (count == 1)
		{
			fscanf(fp, "%d,%d", &x1, &y1);
		}
		else
		{
			if (opcion != 3) {
				fscanf(fp, "%d,%d", &x2, &y2);
				glBegin(GL_LINES);
				glVertex2i(x1, y1);
				glVertex2i(x2, y2);
				glEnd();
				storeEdgeInTable(x1, y1, x2, y2);//storage of edges in edge table. 
			}
			else {
				fscanf(fp, "%d,%d", &x2, &y2);
				glBegin(GL_LINES);
				glTexCoord2i(0, 1); glVertex2i(x1, y1);
				glTexCoord2i(1, 1); glVertex2i(x2, y2);
				glEnd();
				storeEdgeInTable(x1, y1, x2, y2);//storage of edges in edge table. 
			}
		}
	}

}

void archivosDeDibujo() {
	glPushMatrix();
	glTranslatef((GLfloat)midx+350, (GLfloat)midy+300, 0);
	glPushMatrix();
	
	glRotatef((GLfloat)rotarobj, 0.0, 0.0, 1.0);
	glTranslatef((GLfloat)midx, (GLfloat)midy, 0);
	for (int i = 0; i < 7; i++) {
		if (i == 0) {
			fp = fopen("alajuela.txt", "r");
			if (fp == NULL)
			{
				printf("Could not open file alajuela\n");
				return;
			}
			else {
				dibujo();
				if (opcion == 2)
					ScanlineFill(0);

			}
				
			fclose(fp);
		}
		else if (i == 1) {
			fp = fopen("cartago.txt", "r");
			if (fp == NULL)
			{
				printf("Could not open file cartago\n");
				return;
			}
			else {
				dibujo();
				if (opcion == 2)
					ScanlineFill(1);

			}
			fclose(fp);
		}
		else if (i == 2) {
			fp = fopen("guanacaste.txt", "r");
			if (fp == NULL)
			{
				printf("Could not open file guanacaste\n");
				return;
			}
			else {
				dibujo();
				if (opcion == 2)
					ScanlineFill(2);

			}
			fclose(fp);
		}
		else if (i == 3) {
			fp = fopen("heredia.txt", "r");
			if (fp == NULL)
			{
				printf("Could not open file heredia\n");
				return;
			}
			else {
				dibujo();
				if (opcion == 2)
					ScanlineFill(3);

			}
			fclose(fp);
		}
		else if (i == 4) {
			fp = fopen("limon.txt", "r");
			if (fp == NULL)
			{
				printf("Could not open file limon\n");
				return;
			}
			else {
				dibujo();
				if (opcion == 2)
					ScanlineFill(4);

			}
			fclose(fp);
		}
		else if (i == 5) {
			fp = fopen("puntarenas.txt", "r");
			if (fp == NULL)
			{
				printf("Could not open file puntarenas\n");
				return;
			}
			else {
				dibujo();
				if (opcion == 2)
					ScanlineFill(5);

			}
			fclose(fp);
		}
		else {
			fp = fopen("sanjose.txt", "r");
			if (fp == NULL)
			{
				printf("Could not open file sanjose\n");
				return;
			}
			else {
				dibujo();
				if (opcion == 2)
					ScanlineFill(6);

			}
			fclose(fp);
		}
		
	}
	
	glPopMatrix();
	glPopMatrix();
}

void display() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	initEdgeTable();
	if (opcion == 0) {
		glPushMatrix();

		glBindTexture(GL_TEXTURE_2D, nombres_texturas);

		glBegin(GL_POLYGON);
		glTexCoord2i(0, 1); glVertex2i(-100, 400);
		glTexCoord2i(0, 0); glVertex2i(-100, -100);
		glTexCoord2i(1, 0); glVertex2i(450, -100);
		glTexCoord2i(1, 1); glVertex2i(450, 400);
		glEnd();
		glPopMatrix();
	}
	else if(opcion == 2) {
		glDisable(GL_TEXTURE_2D);
		archivosDeDibujo();
	}
	else {
		glEnable(GL_TEXTURE_2D);
		archivosDeDibujo();
	}

	glutSwapBuffers();
}

void teclado(unsigned char key, int x, int y) {
	switch (key) {
	case 27:
		exit(0);
		break;
	case 'x':
		rotarobj = (rotarobj - velRot) % 360;
		break;
	case 'c':
		rotarobj = (rotarobj + velRot) % 360;
		break;
	case 'q':
		left = left + velZoom;
		right = right - velZoom;
		bottom = bottom + velZoom;
		top = top - velZoom;
		glMatrixMode(GL_PROJECTION); // You had GL_MODELVIEW
		glLoadIdentity();
		glOrtho(left, right, bottom, top, nearV, farV); // Changed some of the signs here
		break;
	case 'e':
		left = left - velZoom;
		right = right + velZoom;
		bottom = bottom - velZoom;
		top = top + velZoom;
		glMatrixMode(GL_PROJECTION); // You had GL_MODELVIEW
		glLoadIdentity();
		glOrtho(left, right, bottom, top, nearV, farV); // Changed some of the signs here
		break;
	case 'a':
		left = left - velPan;
		right = right - velPan;
		glMatrixMode(GL_PROJECTION); // You had GL_MODELVIEW
		glLoadIdentity();
		glOrtho(left, right, bottom, top, nearV, farV); // Changed some of the signs here
		break;
	case 'w':
		top = top + velPan;
		bottom = bottom + velPan;
		glMatrixMode(GL_PROJECTION); // You had GL_MODELVIEW
		glLoadIdentity();
		glOrtho(left, right, bottom, top, nearV, farV); // Changed some of the signs here
		break;
	case 'd':
		left = left + velPan;
		right = right + velPan;
		glMatrixMode(GL_PROJECTION); // You had GL_MODELVIEW
		glLoadIdentity();
		glOrtho(left, right, bottom, top, nearV, farV); // Changed some of the signs here
		break;
	case 's':
		top = top - velPan;
		bottom = bottom - velPan;
		glMatrixMode(GL_PROJECTION); // You had GL_MODELVIEW
		glLoadIdentity();
		glOrtho(left, right, bottom, top, nearV, farV); // Changed some of the signs here
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void menu_Zoom(int identificador) {

	switch (identificador) {
	case 0:
		velZoom = 1;
		break;
	case 1:
		velZoom = 2;
		break;
	case 2:
		velZoom = 5;
		break;
	}
}

void menu_Pan(int identificador) {

	switch (identificador) {
	case 0:
		velPan = 1;
		break;
	case 1:
		velPan = 2;
		break;
	case 2:
		velPan = 5;
		break;
	}
}

void menu_Rot(int identificador) {

	switch (identificador) {
	case 0:
		velRot = 5;
		break;
	case 1:
		velRot = 10;
		break;
	case 2:
		velRot = 45;
		break;
	case 3:
		velRot = 90;
		break;
	case 4:
		velRot = 180;
		break;
	}
}

void menu_nivel_1(int identificador) {

	switch (identificador) {
	case 0:
		opcion = 1;
		break;
	case 1:
		opcion = 2;		
		break;
	case 2:
		opcion = 3;
		break;
	case 3:
		opcion = 0;
		glEnable(GL_TEXTURE_2D);
		break;
	case 4:
		exit(-1);
	}
	glutPostRedisplay();
}

void crea_menu() {

	int submenuZoom, submenuPan, submenuRot;

	submenuZoom = glutCreateMenu(menu_Zoom);

	glutAddMenuEntry("x1", 0);
	glutAddMenuEntry("x2", 1);
	glutAddMenuEntry("x5 (Default)", 2);

	submenuPan = glutCreateMenu(menu_Pan);

	glutAddMenuEntry("x1", 0);
	glutAddMenuEntry("x2", 1);
	glutAddMenuEntry("x5 (Default)", 2);

	submenuRot = glutCreateMenu(menu_Rot);

	glutAddMenuEntry("5", 0);
	glutAddMenuEntry("10 (Default)", 1);
	glutAddMenuEntry("45", 2);
	glutAddMenuEntry("90", 3);
	glutAddMenuEntry("180", 4);

	glutCreateMenu(menu_nivel_1);
	glutAddMenuEntry("Sin Colorear", 0);
	glutAddMenuEntry("Coloreado", 1);
	glutAddMenuEntry("Con Texturas", 2);
	glutAddSubMenu("Velocidad del Zoom", submenuZoom);
	glutAddSubMenu("Velocidad del Pan", submenuPan);
	glutAddSubMenu("Grados de Rotación", submenuRot);
	glutAddMenuEntry("Reiniciar", 3);
	glutAddMenuEntry("Salir", 4);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

int main(int argc, char** argv) {

	int id;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(30, 40);
	id = glutCreateWindow("Pali está en toas");

	crea_menu();

	init();
	glutReshapeFunc(reshape);
	glutKeyboardFunc(teclado);
	glutDisplayFunc(display);
	glutMainLoop();

	return 0;
}
